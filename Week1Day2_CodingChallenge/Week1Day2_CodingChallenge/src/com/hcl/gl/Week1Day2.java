package com.hcl.gl;

import java.util.Scanner;

public class Week1Day2 
{
	public static void main(String[] args) 
	{
		//String task;
		int choice;
		Scanner sc=new Scanner(System.in);
		System.out.println("\nEnter the number of task: ");
		int num=sc.nextInt();
		sc.nextLine();
		System.out.println("Enter the tasks for today: ");	    
		String tasks[]=new String[num];
		String newTask;
		for(int i=0;i<num;i++)
		{
			tasks[i]=sc.next();
		}
		
		do
		{
			System.out.println("Enter the operation to be conducted: ");
			System.out.println("1. INSERT\n2. UPDATE\n3. DELETE\n4. SEARCH \n5. VIEW ALL \n 0. Exit");
			choice=sc.nextInt();
			switch(choice)
			{
			case 1: 
				System.out.println("Add elements into the array: ");
				String demoTasks[]=new String[tasks.length+1];
				System.out.println("Enter the new tasks to be added into the array:");
				newTask=sc.next();
				for(int i=0;i<num;i++)
				{
					demoTasks[i]=tasks[i];                   //OLD TASK ADDED
				}
				demoTasks[demoTasks.length-1]=newTask;
				tasks=new String[demoTasks.length];
				for(int i=0;i<demoTasks.length;i++)
				{
					tasks[i]=demoTasks[i];
				}
//				for(int i=0;i<num+1;i++)
//				{
//					System.out.println(demoTasks[i]);
//				}
				for(int i=0;i<tasks.length;i++)
				{
					System.out.println(tasks[i]);
				}
				break;
			case 2:
				System.out.println("Update elements within the array");
				System.out.println("Enter the task to be updated: ");
				int flag=0, size=0;
				String taskValue=sc.next();
				for(int x=0;x<tasks.length;x++)
				{
					if(taskValue.equalsIgnoreCase(tasks[x]))
					{
						System.out.println("Enter the required updated task:");
						tasks[x]=sc.next();
						System.out.println("Task Successfully Updated ");
						flag=1;
						break;
					}
				}
				if(flag==0)
				{
					System.out.println("Task not found");
				}
				break;
			case 3:
				System.out.println("Delete elements from the array");
				flag=0;
				System.out.println("Enter the task to be deleted: ");
				taskValue=sc.next();
				for(int j=0;j<tasks.length;j++)
				{
					if(taskValue.equalsIgnoreCase(tasks[j]))
					{
						
						tasks[j]=null;
//						for(int k=j;k<tasks[k].length();k++)
//						{
//							//tasks[k]=tasks[k+1];
//							tasks[k]=null;
//						}
						//size=tasks[j].length();
						//size--;
						flag=1;
						System.out.println("Task Deleted Successfully");
					}
					
				}
				
				if(flag==0)
				{
					System.out.println("Task not found");
				}

				break;

			case 4:
				System.out.println("Search elements from the array");
				flag=0;
				System.out.println("Enter a task to be searched: ");
				taskValue=sc.next();
				for(int j=0;j<tasks.length;j++)
				{
					if(taskValue.equalsIgnoreCase(tasks[j]))
					{	
						flag=1;
						System.out.println("Task matched: Title: "+tasks[j]);
					}
				}
				if(flag==0)
				{
					System.out.println("Task Not found");
				}
				break;
			case 5:
				System.out.println("Elements from the array");
				for(int j=0;j<tasks.length;j++)
				{
					System.out.println(tasks[j]);
				}
			case 0: System.out.println("You have exited the application");
			}
		}
		while(choice!=0);
	}

}

