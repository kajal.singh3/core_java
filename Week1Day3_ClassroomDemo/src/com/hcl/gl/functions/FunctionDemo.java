package com.hcl.gl.functions;

public class FunctionDemo 
{
	int num1=12;  //INSTANCE/GLOBAL VARIABLE: outside the functions,
											 //but inside the class
	
	public void add()    //FUNCTION DEFINITION   //FUNCTION BODY
	{
		System.out.println("Hi Today is: "+num1);
	}
	public void greet()
	{
		System.out.println("Good morning");
	}
	
	public static void main(String[] g)
	{
      System.out.println("HELLO");
      FunctionDemo obj=new FunctionDemo();
      obj.add();
      obj.greet();
	}
}
