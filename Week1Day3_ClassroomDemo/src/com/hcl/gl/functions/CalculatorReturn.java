package com.hcl.gl.functions;

import java.util.Scanner;

public class CalculatorReturn 
{
   public float add(int a,int b)
   {
	   float z=a+b;
	  return z;   //SENDING DATA
   }
 
   public String greet()
   {
	  return "Good Day!";
   }
   
   public static void main(String[] args) 
   {
	   //main: is not considered as a part of class
	   //
	    CalculatorReturn calcObj=new CalculatorReturn();
	    
	    int answer=(int)calcObj.add(12,45);
	    System.out.println(answer);
	 
	    System.out.println(calcObj.greet());
	    
//	    String data=calcObj.greet();
//	    System.out.println(data);
   }
}
