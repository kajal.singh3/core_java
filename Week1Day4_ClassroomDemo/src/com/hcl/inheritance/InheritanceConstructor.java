package com.hcl.inheritance;

import java.util.Scanner;

class Vehicle1  //BASE CLASS
{
	int speed=12;
	public Vehicle1()
	{
		System.out.println("Base Class Executed");
	}
	public Vehicle1(String msg)
	{
		
		System.out.println(msg);
	}
	
}
//ONLY DERIVED CLASS FUNCTION
class FourWheeler1 extends Vehicle1   //DERIVED CLASS
{
	int speed=13;
	public FourWheeler1()
	{
		
		
		System.out.println("Derived Class executed "+speed+" "+super.speed);
	}	
}	
public class InheritanceConstructor
{
	public static void main(String args[])
	{
		FourWheeler1 fourWheeler=new FourWheeler1();
		
		
	}
}
