package com.hcl.inheritance;

import java.util.*;

class Vehicle  //BASE CLASS
{
	int speed;
	Scanner sc=new Scanner(System.in);
	void getData()
	{
		speed=sc.nextInt();
		System.out.println("GetData of Base class");
	}
}
//ONLY DERIVED CLASS FUNCTION
class FourWheeler extends Vehicle   //DERIVED CLASS
{
	void getData()
	{
		super.getData();
		System.out.println("GetData of derived class");
	}
	public void display()
	{
		System.out.println("Speed: "+speed);
	}
	
}	
public class InheritanceDemo 
{
	public static void main(String args[])
	{
		FourWheeler fourWheeler=new FourWheeler();
		fourWheeler.getData();
		fourWheeler.display();
	}
}
