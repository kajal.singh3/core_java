package com.hcl.gl.constructors;

public class UserDao 
{
	
	//AN OBJECT IS CREATED
	int x=12;
	public UserDao() //WITHOUT PARAMETER: Default Constructor  
	{
		System.out.println("Hi it's "+x);
	}
	public UserDao(float z) //WITHOUT PARAMETER: Default Constructor  
	{
		System.out.println("Hi it's "+x);
	}
	
	public UserDao(int data)
	{
		System.out.println("Parameterized Constructor: "+data);
	}

	public UserDao(int a, int b)
	{
		System.out.println("a "+a+" b "+b);
	}
	public UserDao(float x, float y)
	{
		System.out.println("x "+x+" y "+y);
	}
	public static void main(String[] args) 
	{
		UserDao userDao=new UserDao();
		UserDao userDao1=new UserDao(10);
		UserDao userDao2=new UserDao(14,16);
		UserDao userDao4=new UserDao(12.5f,13.0f);
	}
	
}
