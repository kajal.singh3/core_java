package com.hcl.gl.constructors;

//WHEN ONE OR MORE FUNCTIONS have the same name but different number or type of parameters: METHOD/FUNCTION OVERLOADING

public class UserDaoMethod 
{
	void getData()    //DEAFULT METHOD
	{
		System.out.println("getData() called");
	}
	void getData(int x)   //PARAMETERIZED
	{
		System.out.println("getData(int x) called "+x);
	}
	void getData(float x)   //PARAMETERIZED
	{
		System.out.println("getData(int x) called "+x);
	}
	void getData(int x,int y)   //PARAMETERIZED
	{
		System.out.println("getData(int x) called "+x);
	}
	public static void main(String[] args) 
	{
		UserDaoMethod user=new UserDaoMethod();   //DEFAULT: background
		user.getData();
		user.getData(14);
		user.getData(14, 16);
	}
}

//WHENEVER YOU CREATE an OBJECT, CONSTRUCTOR IS EXECUTED: DEFAULT 