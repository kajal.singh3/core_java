MECHANISM of DERIVING a new class from an existing class: INHERITANCE


OLD/EXISTING: BASE CLASS/PARENT CLASS/SUPER CLASS


NEW CLASS: DERIVED/CHILD/SUB CLASS

## METHOD OVERRIDING

to call the base class

super.function_name()/variable_name;


class Messaging
{
   int a;
}
class MessagingVoice extends Messaging 
{
	int b;
}


class BaseClassName
{

}
class DerivedClassName extends BaseClassName
{


}




METHOD OVERRIDING
super keyword

Base Class:

getData()

Derived Class:
getData()
display()






























