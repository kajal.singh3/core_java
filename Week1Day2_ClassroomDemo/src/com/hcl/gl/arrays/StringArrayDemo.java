package com.hcl.gl.arrays;
import java.util.*;
public class StringArrayDemo 
{
	public static void main(String args[]) {
		String names[] = {"HCL", "GL", "Infosys", "Wipro"};
		Scanner sc = new Scanner(System.in);
		String name1;
		System.out.println("Enter the name to be searched");
		name1 = sc.next();
		for(int i = 0; i<names.length; i++) {
			if(name1.equalsIgnoreCase(names[i])) {
				System.out.println("Name is at position : "+i);
				break;
			}
		}
	}
}
