package com.hcl.gl.arrays;

import java.util.Scanner;

public class MultidimensionalArraysDemo 
{
	public static void main(String args[]) {
		int a[][] = new int[2][3];
		Scanner sc = new Scanner(System.in);
		for(int i = 0; i<2;i++) {
			for(int j = 0; j<3; j++) {
				System.out.println("Please enter the number");
				a[i][j] = sc.nextInt();
				sc.nextLine();
			}
		}
		for(int i = 0; i<2; i++) {
			for(int j = 0; j<3; j++) {
				System.out.print(" "+a[i][j]+" ");
			}
			System.out.println("");
		}
	}

}


