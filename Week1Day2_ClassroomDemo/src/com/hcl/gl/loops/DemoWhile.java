
package com.hcl.gl.loops;

public class DemoWhile 
{
	public static void main(String[] args) 
	{
		int i=0; //initilization
		while(i<10)  //condition
		{	
					//increment
			if(i==4)
			{
				i++;	
				//break;
				continue;
			}
			System.out.println(i);	
			i++;
		}
		System.out.println("Works!!");

	}
}

