package com.hcl.gl.loops;

public class DemoDoWhile 
{
	public static void main(String[] args) 
	{
		   int i=10;
           do
            {
            	System.out.println("Value of i: "+i);
            	i++;
            }
           while(i<20);
	}
}
