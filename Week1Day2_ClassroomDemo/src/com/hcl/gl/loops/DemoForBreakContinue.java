package com.hcl.gl.loops;

public class DemoForBreakContinue 
{
	public static void main(String[] args)
	{
		for(int i=1;i<=5;i++)
		{
			if(i==3 || i==5)        
			{
				System.out.println("Hi All "+i);
			}
			System.out.println("This is for loop "+i);
		}
		System.out.println("Works!!");
		
	}
}

 
//SYNTAX:
//for(initialization;condition;increment/decrement)
//{
//	//BLOCK OF CODE
//}

//-0 to 4
//0
//1
//2
//3
//4