package com.hcl.gl.statement;

import java.util.Scanner;

public class VolumeCalculator 
{
  public static void main(String[] args) 
  {
	int volume, length, breadth, height;
	
	Scanner obj=new Scanner(System.in);
	System.out.print("Enter l,b,h: ");
	length=obj.nextInt();
	breadth=obj.nextInt();
	height=obj.nextInt();
	
	volume=length*breadth*height;
	System.out.println("Volume is: "+volume);
  }
}
