package com.hcl.gl.statement;

import java.util.Scanner;

public class UserInput {

	public static void main(String[] args) 
	{
	    String name,msg;
	    int age;
	    float salary;
	    
	    Scanner sc=new Scanner(System.in);
	    System.out.println("Enter your name: ");
	    name=sc.next();	   //next() does not take any space: single word
	    					//nextLine: takes entire statement
	    
	    System.out.println("Enter your age: ");
	    age=sc.nextInt();
	    
	    sc.nextLine();
	    
	    System.out.println("Enter your msg: ");
	    msg=sc.nextLine();
	   
	    
	    System.out.println("Enter your salary: ");
	    salary=sc.nextFloat();
	   
	    
	    System.out.println("*****************Details are: *****************");
	    System.out.println("Name: "+name+"\nAge: "+age+"\nSalary: "+salary+"\n Message is: "+msg);   // \n means new line
	    
	}

}
