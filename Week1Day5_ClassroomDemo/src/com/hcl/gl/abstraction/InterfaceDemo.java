package com.hcl.gl.abstraction;


class Vehicle extends AbstractClassDemo implements FourWheeler   //INHERIT AN interface
{
	public void getDemo()
	{
		System.out.println("Abstract class");
	}
	
	public void getSpeed()     //NON ABSTRACT METHOD
	{
		
		System.out.println("4 Speed is "+FourWheeler.speed);
		System.out.println("2 Speed is "+TwoWheeler.speed);
	}
	public void sayHi()
	{
		System.out.println("HI");
	}
}


public class InterfaceDemo 
{
  public static void main(String[] args) 
  {
	  
	Vehicle vehicleObj=new Vehicle();
	vehicleObj.getSpeed();
	vehicleObj.sayHi();
	vehicleObj.getDemo();
	vehicleObj.getGreet();
  }
}
