#Create projects in Eclipse:
1. File Tab-> New -> Other -> Java Project

POP-UP: ProjectName  -> Next -> Finish
Module.info: Don't create
Open Perspective: No

Project
- JRE System Library
-  src (main folder with all java files)

#Create a java file
Right Click on 'src' folder -> Select 'New' ->
Select -> 'Class'
Pop-Up: Name of the Class -> 'Finish'

#Run a project
Right click on filename -> Run As -> Java Application
Output visible on console

#Variable
datatype variable-name=value;
RULES WHENEVER YOU CREATE A VARIABLE:
- variable can contain only letters, numbers, dollar sign or underscore;
- it cannot be a keyword 
- a variable should always start with a letter/dollarsign/underscore
   Never start it with a number
- No whitespaces are allowed
- Variable names are cases sensitive


BEST PRACTICES (Naming Convention):
- always start with variable name with small letter.
- If there is more than one word in variable name then the second word 1st letter should be capital
- Give a valid name

# DATATYPES:
## PRIMITIVE DATATYPES: predefined datatypes
- they have a fixed memory
int, float, double, short, long, byte, boolean, char

## NON-PRIMITIVE DATAYPES: not predefined
String: Class
Class
Arrays
Enum











both the task are same: 0
if task1>task 2: positive: >0
if task1<task2: negative number: <0


String task1: Performing coding
String task2: attending sessions
String task3: Coding

String temp;

if((task1.compareTo(task2))>0)
{
   temp=task1;     //temp="PC"
   task1=task2;    //task1="Attending sessions"
   task2=temp;     //task2="Performing coding"
}

task1="Attending sessions"
task2="Performing coding"
task3="Coding"
if((task1.compareTo(task3))>0)       task1="Attending session"       task3=
{
   temp=task1;     //temp="PC"
   task1=task3;    //task1="Attending sessions
   task3=temp;     
}
if(task2.compareTo(task3)>0)
{
   temp=task2;     
   task2=task3;   
   task3=temp;  
}

































